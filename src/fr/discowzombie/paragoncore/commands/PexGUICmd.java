package fr.discowzombie.paragoncore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.discowzombie.paragoncore.menus.InventoryManager;
import fr.discowzombie.paragoncore.menus.inv.MainMenu;

public class PexGUICmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			if((p.hasPermission("pexgui.open")) || (p.isOp())){
				p.sendMessage("§aPréparation du menu... Cela peut prendre quelques secondes...");
				new InventoryManager().open(p, MainMenu.class);
				p.sendMessage("§aOuverture...");
			}
		}
		return true;
	}

}
