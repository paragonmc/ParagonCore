package fr.discowzombie.paragoncore.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.discowzombie.paragoncore.ParagonCore;

public class RulesCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			List<String> lines = ParagonCore.getInstance().getConfig().getStringList("rules");
			for(String s : lines){
				p.sendMessage(s.replace('&', '§'));
			}
		}
		return true;
	}

}
