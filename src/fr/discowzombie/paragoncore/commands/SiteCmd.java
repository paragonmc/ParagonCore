package fr.discowzombie.paragoncore.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.discowzombie.paragoncore.ParagonCore;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

public class SiteCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			
			TextComponent site = new TextComponent();
			site.setText(ParagonCore.getInstance().getConfig().getString("command.site").replace('&', '§'));
			site.setClickEvent(new ClickEvent(Action.OPEN_URL, "http://www.paragonmc.fr/"));
			
			TextComponent boutique = new TextComponent();
			boutique.setText(ParagonCore.getInstance().getConfig().getString("command.boutique").replace('&', '§'));
			boutique.setClickEvent(new ClickEvent(Action.OPEN_URL, "http://shop.paragonmc.fr/"));
			
			List<String> lines = ParagonCore.getInstance().getConfig().getStringList("site");
			for(String s : lines){
				if(s.contains("%site%")) p.spigot().sendMessage(site);
				else if(s.contains("%boutique%")) p.spigot().sendMessage(boutique);
				else p.sendMessage(s.replace('&', '§'));
			}
		}
		return true;
	}

}
