package fr.discowzombie.paragoncore.commands;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.discowzombie.paragoncore.ParagonCore;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.ClickEvent.Action;

public class IPCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			
			TextComponent ts = new TextComponent();
			ts.setText(ParagonCore.getInstance().getConfig().getString("command.ts").replace('&', '§'));
			ts.setClickEvent(new ClickEvent(Action.OPEN_URL, "http://www.teamspeak.com/invite/ts.paragonmc.fr/"));
			
			TextComponent discord = new TextComponent();
			discord.setText(ParagonCore.getInstance().getConfig().getString("command.discord").replace('&', '§'));
			discord.setClickEvent(new ClickEvent(Action.OPEN_URL, "https://discordapp.com/invite/rxZd8CY"));
			
			List<String> lines = ParagonCore.getInstance().getConfig().getStringList("ips");
			for(String s : lines){
				if(s.contains("%ts%")) p.spigot().sendMessage(ts);
				else if(s.contains("%discord%")) p.spigot().sendMessage(discord);
				else p.sendMessage(s.replace('&', '§'));
			}
		}
		return true;
	}

}
