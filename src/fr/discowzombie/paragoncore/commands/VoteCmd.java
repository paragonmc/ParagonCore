package fr.discowzombie.paragoncore.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.discowzombie.paragoncore.menus.InventoryManager;
import fr.discowzombie.paragoncore.menus.inv.VoteMenu;

public class VoteCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String str, String[] args) {
		if(sender instanceof Player){
			Player p = (Player) sender;
			new InventoryManager().open(p, VoteMenu.class);
		}
		return true;
	}

}
