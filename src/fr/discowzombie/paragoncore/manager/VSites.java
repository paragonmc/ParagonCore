package fr.discowzombie.paragoncore.manager;

public enum VSites {
	
	SITE1("site1", 24, "http://www.serveurs-minecraft.org/vote.php?id=52301", "Serveur-Minecraft.org"),
	SITE2("site2", 24, "https://www.serveurs-minecraft.com/serveur-minecraft?Classement=Paragon", "Serveur-Minecraft.com"),
	SITE3("site3", 24, "www.paragonmc.fr", ""),
	SITE4("site4", 3, "www.paragonmc.fr", ""),
	SITE5("site5", 1, "www.paragonmc.fr", ""),
	;
	
	private String name, url, sitename;
	private long delay;//EN HEURES
	
	private VSites(String name, long delay, String url, String sitename) {
        this.name = name;
        this.delay = delay;
        this.url = url;
        this.sitename = sitename;
    }
	
	public String getName(){
		return name;
	}
	
	public long getDelay(){
		return delay;
	}
	
	public String getUrl(){
		return url;
	}
	
	public String getSiteName(){
		return sitename;
	}

}
