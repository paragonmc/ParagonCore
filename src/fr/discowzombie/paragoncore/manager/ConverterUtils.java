package fr.discowzombie.paragoncore.manager;

import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class ConverterUtils {
	
	/*
	 * 
	 */
	/*
	 * 
	 */
	public static String toTrueTimeFormat(long l){
		Date date = new Date(l);
		DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String dateFormatted = formatter.format(date);
		return dateFormatted;
	}
	
	/*
	 * 
	 */
	/*
	 * 
	 */
	public static String toReadableTimeFormat(long l){
		long second = (l / 1000) % 60;
		long minute = (l / (1000 * 60)) % 60;
		long hour = (l / (1000 * 60 * 60)) % 24;
		String s = "";
		
		if(hour >= 1){
			s = "§f"+hour+" §7heures";
			if(minute > 0){
				s = s + "§7, §f"+minute+" §7minutes";
				if(second > 0){
					s = s + "§7, §f"+second+" §7secondes";
				}
			}else if(second > 0){
				s = s + "§7, §f"+second+" §7secondes";
			}
			s = s + "§7.";
			return s;
		}
		
		if(minute >= 1){
			s = "§f"+minute+" §7minutes";
			if(second > 0){
				s = s + "§7, §f"+second+" §7secondes";
			}
			s = s +"§7.";
			return s;	
		}
		
		if(second >= 1){
			s = "§f"+second+" §7secondes.";
			return s;
		}
		
		return null;
		//String time = String.format("%02d:%02d:%02d:%d", hour, minute, second, millis);
	}
	
	/*
	 * 
	 */
	/*
	 * 
	 */
	public static String convertISAtoString(InetSocketAddress ips){
		String ip;
		if(ips.toString().contains("-")){
			ip = ips.toString().replace("/", "").replace(ips.getHostName(), "").replace(":", "").replace(ips.getPort()+"", "");
		}else{
			ip = ips.toString().replace("/", "").replace(":", "").replace(ips.getPort()+"", "");
		}
		return ip;
	}

}
