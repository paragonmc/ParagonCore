package fr.discowzombie.paragoncore.manager;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import fr.discowzombie.paragoncore.ParagonCore;

public enum VoteRewards {
	
	r1(1),
	r2(3),
	r3(5),
	r4(10),
	r5(15),
	r6(20),
	r7(30),
	r8(40),
	r9(50),
	r10(70),
	r11(90),
	r12(110),
	r13(130),
	r14(160),
	r15(190),
	r16(220),
	r17(250),
	r18(280),
	r19(310),
	r20(340),
	r21(370),
	;
	
	private int voteNeed;
	
	private VoteRewards(int voteNeed){
		this.voteNeed = voteNeed;
	}
	
	public int getVoteNeed(){
		return voteNeed;
	}
	
	public static VoteRewards intToVRewards(int i){
		switch (i) {
		case 1: return VoteRewards.r1;
		case 3: return VoteRewards.r2;
		case 5: return VoteRewards.r3;
		case 10: return VoteRewards.r4;
		case 15: return VoteRewards.r5;
		case 20: return VoteRewards.r6;
		case 30: return VoteRewards.r7;
		case 40: return VoteRewards.r8;
		case 50: return VoteRewards.r9;
		case 70: return VoteRewards.r10;
		case 90: return VoteRewards.r11;
		case 110: return VoteRewards.r12;
		case 130: return VoteRewards.r13;
		case 160: return VoteRewards.r14;
		case 190: return VoteRewards.r15;
		case 220: return VoteRewards.r16;
		case 250: return VoteRewards.r17;
		case 280: return VoteRewards.r18;
		case 310: return VoteRewards.r19;
		case 340: return VoteRewards.r20;
		case 370: return VoteRewards.r21;
		default:
			break;
		}
		return null;
	}
	
	public static String getRewardsName(VoteRewards vr){
		String s = "";
		switch (vr) {
		case r1: s = ParagonCore.getInstance().getConfig().getString("votes.1.name").replace('&', '§'); break;
		case r2: s = ParagonCore.getInstance().getConfig().getString("votes.3.name").replace('&', '§'); break;
		case r3: s = ParagonCore.getInstance().getConfig().getString("votes.5.name").replace('&', '§'); break;
		case r4: s = ParagonCore.getInstance().getConfig().getString("votes.10.name").replace('&', '§'); break;
		case r5: s = ParagonCore.getInstance().getConfig().getString("votes.15.name").replace('&', '§'); break;
		case r6: s = ParagonCore.getInstance().getConfig().getString("votes.20.name").replace('&', '§'); break;
		case r7: s = ParagonCore.getInstance().getConfig().getString("votes.30.name").replace('&', '§'); break;
		case r8: s = ParagonCore.getInstance().getConfig().getString("votes.40.name").replace('&', '§'); break;
		case r9: s = ParagonCore.getInstance().getConfig().getString("votes.50.name").replace('&', '§'); break;
		case r10: s = ParagonCore.getInstance().getConfig().getString("votes.70.name").replace('&', '§'); break;
		case r11: s = ParagonCore.getInstance().getConfig().getString("votes.90.name").replace('&', '§'); break;
		case r12: s = ParagonCore.getInstance().getConfig().getString("votes.110.name").replace('&', '§'); break;
		case r13: s = ParagonCore.getInstance().getConfig().getString("votes.130.name").replace('&', '§'); break;
		case r14: s = ParagonCore.getInstance().getConfig().getString("votes.160.name").replace('&', '§'); break;
		case r15: s = ParagonCore.getInstance().getConfig().getString("votes.190.name").replace('&', '§'); break;
		case r16: s = ParagonCore.getInstance().getConfig().getString("votes.220.name").replace('&', '§'); break;
		case r17: s = ParagonCore.getInstance().getConfig().getString("votes.250.name").replace('&', '§'); break;
		case r18: s = ParagonCore.getInstance().getConfig().getString("votes.280.name").replace('&', '§'); break;
		case r19: s = ParagonCore.getInstance().getConfig().getString("votes.310.name").replace('&', '§'); break;
		case r20: s = ParagonCore.getInstance().getConfig().getString("votes.340.name").replace('&', '§'); break;
		case r21: s = ParagonCore.getInstance().getConfig().getString("votes.370.name").replace('&', '§'); break;
		default:
			break;
		}
		return s;
	}
	
	public static void giveReward(VoteRewards vr, Player p){
		switch (vr) {
		case r1: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.1.cmd").replace("%player%", p.getName())); break;
		case r2: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.3.cmd").replace("%player%", p.getName())); break;
		case r3: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.5.cmd").replace("%player%", p.getName())); break;
		case r4: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.10.cmd").replace("%player%", p.getName())); break;
		case r5: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.15.cmd").replace("%player%", p.getName())); break;
		case r6: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.20.cmd").replace("%player%", p.getName())); break;
		case r7: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.30.cmd").replace("%player%", p.getName())); break;
		case r8: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.40.cmd").replace("%player%", p.getName())); break;
		case r9: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.50.cmd").replace("%player%", p.getName())); break;
		case r10: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.70.cmd").replace("%player%", p.getName())); break;
		case r11: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.90.cmd").replace("%player%", p.getName())); break;
		case r12: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.110.cmd").replace("%player%", p.getName())); break;
		case r13: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.130.cmd").replace("%player%", p.getName())); break;
		case r14: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.160.cmd").replace("%player%", p.getName())); break;
		case r15: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.190.cmd").replace("%player%", p.getName())); break;
		case r16: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.220.cmd").replace("%player%", p.getName())); break;
		case r17: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.250.cmd").replace("%player%", p.getName())); break;
		case r18: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.280.cmd").replace("%player%", p.getName())); break;
		case r19: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.310.cmd").replace("%player%", p.getName())); break;
		case r20: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.340.cmd").replace("%player%", p.getName())); break;
		case r21: Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), ParagonCore.getInstance().getConfig().getString("votes.370.cmd").replace("%player%", p.getName())); break;
		default:
			break;
		}
	}

}
