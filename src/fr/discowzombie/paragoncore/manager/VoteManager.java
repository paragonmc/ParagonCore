package fr.discowzombie.paragoncore.manager;

import java.util.UUID;

import fr.discowzombie.paragoncore.ParagonCore;

public class VoteManager {
	
	private UUID uuid;
	
	public VoteManager(UUID uuid){
		this.uuid = uuid;
	}
	
	public boolean hasVoteStats(){
		if(ParagonCore.getInstance().getConfig().contains("votes."+uuid)){
			return true;
		}
		return false;
	}
	
	public int getVotes(){
		if(!hasVoteStats()){
			createVoteStats();
		}
		
		int i = ParagonCore.getInstance().getConfig().getInt("votes."+uuid);
		return i;
	}
	
	public void createVoteStats() {
		if(!hasVoteStats()){
			ParagonCore.getInstance().getConfig().set("votes."+uuid, 0);
			for(VSites v : VSites.values()){
				ParagonCore.getInstance().getConfig().set("votes."+v.getName()+"."+uuid+".last", 0);
			}
			for(VoteRewards vr : VoteRewards.values()){
				ParagonCore.getInstance().getConfig().set("votes."+vr.getVoteNeed()+"."+uuid, false);
			}
			ParagonCore.getInstance().saveConfig();
		}
	}

	public void addVote(){
		if(!hasVoteStats()){
			createVoteStats();
		}
		
		int i = getVotes();
		ParagonCore.getInstance().getConfig().set("votes."+uuid, i+1);
		ParagonCore.getInstance().saveConfig();
	}
	
	// -- ADVANCED -- //
	
	public long getLastVote(VSites vsites){
		long lastVote = ParagonCore.getInstance().getConfig().getLong("votes."+vsites.getName()+"."+uuid+".last");
		return lastVote;
	}
	
	public boolean canVote(VSites vsites){
		long now = System.currentTimeMillis();
		long after = getLastVote(vsites) + (vsites.getDelay() * 60 * 60 * 1000);
		if(now >= after){
			return true;
		}
		return false;
	}
	
	public long canVoteIn(VSites vsites){
		if(!canVote(vsites)){
			long now = System.currentTimeMillis();
			long after = getLastVote(vsites) + (vsites.getDelay() * 60 * 60 * 1000);
			if(now < after){
				long calcul = after - now;
				return calcul;
			}
		}
		return -1;
	}
	
	public void setLastVote(VSites vsites){
		if(canVote(vsites)){
			ParagonCore.getInstance().getConfig().set("votes."+vsites.getName()+"."+uuid+".last", System.currentTimeMillis());
			ParagonCore.getInstance().saveConfig();
		}
	}
	
	// -- Rewards -- //
	public boolean hadCollected(VoteRewards vr){
		boolean b = ParagonCore.getInstance().getConfig().getBoolean("votes."+vr.getVoteNeed()+"."+uuid);
		return b;
	}
	
	public void setComplete(VoteRewards vr){
		if(!hadCollected(vr)){
			ParagonCore.getInstance().getConfig().set("votes."+vr.getVoteNeed()+"."+uuid, true);
			ParagonCore.getInstance().saveConfig();
		}
	}

}
