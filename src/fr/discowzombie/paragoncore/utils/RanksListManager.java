package fr.discowzombie.paragoncore.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class RanksListManager {
	
	public static Map<PermissionGroup, Integer> m = null;
	
	public RanksListManager(){}
	
	public Map<PermissionGroup, Integer> getRanks(){
		if((m == null) || (m.isEmpty())){
			m = new TreeMap<>();
			HashMap<PermissionGroup, Integer> mp = new HashMap<>();
			for(PermissionGroup pg : PermissionsEx.getPermissionManager().getGroupList()){
				mp.put(pg, pg.getWeight());
			}
			m = ComparatorManager.sortByValues(mp);
		}
		return m;
	}

}
