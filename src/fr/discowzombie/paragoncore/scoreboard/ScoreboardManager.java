package fr.discowzombie.paragoncore.scoreboard;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Vector;

import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.wasteofplastic.askyblock.ASkyBlockAPI;

import fr.discowzombie.paragoncore.ParagonCore;
import fr.discowzombie.paragoncore.manager.VoteManager;

public class ScoreboardManager implements Listener{

	public static Map<Player, ScoreboardSign> boards = new HashMap<>();
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event){
		Player player = event.getPlayer();
		if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
			setBoard(player);
			updateBoard();
		}
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent event){
		Player player = event.getPlayer();
		if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
			updateBoard();
			removeBoard(player);
		}
	}
	
	public static void removeBoard(Player player){
		if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
			if(boards.containsKey(player)){
				boards.get(player).destroy();
			}
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void setBoard(Player player){
		if(boards.containsKey(player))
			boards.remove(player);
		ScoreboardSign scoreboard = new ScoreboardSign(player, "skyblock");
		scoreboard.create();
		scoreboard.setObjectiveName("§6§lSky§e§lBlock");
		scoreboard.setLine(0, "§f");
		scoreboard.setLine(1, "§7──────────");
		scoreboard.setLine(2, "§3Challenge §7: §a");
		scoreboard.setLine(3, "§3Niveau §7: §a"+ASkyBlockAPI.getInstance().getIslandLevel(player.getUniqueId()));
		int size = ASkyBlockAPI.getInstance().getTeamMembers(player.getUniqueId()).size();
		if(size == 0 && ASkyBlockAPI.getInstance().hasIsland(player.getUniqueId())) size = 1;
		scoreboard.setLine(4, "§3Membres §7: §a"+size);
		scoreboard.setLine(5, "§c§7──────────");
		scoreboard.setLine(6, "§3Mort §7: §a"+player.getStatistic(Statistic.DEATHS));
		scoreboard.setLine(7, "§3Kill §7: §a"+player.getStatistic(Statistic.PLAYER_KILLS));
		scoreboard.setLine(8, "§3Mob kill §7: §a"+player.getStatistic(Statistic.MOB_KILLS));
		scoreboard.setLine(9, "§d§7──────────");
		scoreboard.setLine(10, "§3Présence §7: §a"+(player.getStatistic(Statistic.PLAY_ONE_TICK) / 72000)+"§ah");
		String money = String.format("%,.0f", ParagonCore.getEcononomy().getBalance(player));
		scoreboard.setLine(11, "§3Money §7: §a"+money);
		int votes = new VoteManager(player.getUniqueId()).getVotes();
		scoreboard.setLine(12, "§3Votes §7: §a"+votes);
		scoreboard.setLine(13, "§f§7──────────");
		boards.put(player, scoreboard);
	}
	
	public static String[] customSplit(String src, int size) {
	    Vector<String> vec = new Vector<String>();
	    for(int i = 0; i < src.length(); i += size) {
	        vec.add(src.substring(i, (i+size > src.length()) ? src.length() : i+size));
	    }
	    return (String[]) vec.toArray(); 
	}
	
	@SuppressWarnings("deprecation")
	public static void updateBoard(){
		for(Entry<Player, ScoreboardSign> sign : boards.entrySet()){
			Player player = sign.getKey();
			sign.getValue().setLine(2, "§3Challenge §7: §a");
			sign.getValue().setLine(3, "§3Niveau §7: §a"+ASkyBlockAPI.getInstance().getIslandLevel(player.getUniqueId()));
			int size = ASkyBlockAPI.getInstance().getTeamMembers(player.getUniqueId()).size();
			if(size == 0 && ASkyBlockAPI.getInstance().hasIsland(player.getUniqueId())) size = 1;
			sign.getValue().setLine(4, "§3Membres §7: §a"+size);
			sign.getValue().setLine(6, "§3Mort §7: §a"+player.getStatistic(Statistic.DEATHS));
			sign.getValue().setLine(7, "§3Kill §7: §a"+player.getStatistic(Statistic.PLAYER_KILLS));
			sign.getValue().setLine(8, "§3Mob kill §7: §a"+player.getStatistic(Statistic.MOB_KILLS));
			sign.getValue().setLine(10, "§3Présence §7: §a"+(player.getStatistic(Statistic.PLAY_ONE_TICK) / 72000)+"§ah");
			String money = String.format("%,.0f", ParagonCore.getEcononomy().getBalance(player));
			sign.getValue().setLine(11, "§3Money §7: §a"+money);
			int votes = new VoteManager(player.getUniqueId()).getVotes();
			sign.getValue().setLine(12, "§3Votes §7: §a"+votes);
		}
	}

}