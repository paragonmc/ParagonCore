package fr.discowzombie.paragoncore.heads;

public enum Alphabet {
	
	ARROW_UP("Arrow Up", "http://textures.minecraft.net/texture/d48b768c623432dfb259fb3c3978e98dec111f79dbd6cd88f21155374b70b3c"),
	ARROW_DOWN("Arrow Down", "http://textures.minecraft.net/texture/2dadd755d08537352bf7a93e3bb7dd4d733121d39f2fb67073cd471f561194dd"),
	ARROW_RIGHT("Arrow Right", "http://textures.minecraft.net/texture/1b6f1a25b6bc199946472aedb370522584ff6f4e83221e5946bd2e41b5ca13b"),
	ARROW_LEFT("Arrow Left", "http://textures.minecraft.net/texture/3ebf907494a935e955bfcadab81beafb90fb9be49c7026ba97d798d5f1a23"),
	QUESTION("Question", "http://textures.minecraft.net/texture/5163dafac1d91a8c91db576caac784336791a6e18d8f7f62778fc47bf146b6"),
	V("V", "http://textures.minecraft.net/texture/dce27a153635f835237d85c6bf74f5b1f2e638c48fee8c83038d0558d41da7"),
	O("O", "http://textures.minecraft.net/texture/88445466bdc5ad5bcea82239c4e1b510f6ea5262d82d8a96d7291c342fb89"),
	T("T", "http://textures.minecraft.net/texture/64c75619b91d241f678350ad9237c134c5e08d87d6860741ede306a4ef91"),
	E("E", "http://textures.minecraft.net/texture/db251487ff8eef2ebc7a57dab6e3d9f1db7fc926ddc66fea14afe3dff15a45"),
	S("S", "http://textures.minecraft.net/texture/60d09dfd9f5de6243233e0e3325b6c3479335e7ccf13f2448d4e1f7fc4a0df"),
	Facebook("Facebook", "http://textures.minecraft.net/texture/deb46126904463f07ecfc972aaa37373a22359b5ba271821b689cd5367f75762"),
	Twitter("Twitter", "http://textures.minecraft.net/texture/3685a0be743e9067de95cd8c6d1ba21ab21d37371b3d597211bb75e43279"),
	Youtube("Youtube", "http://textures.minecraft.net/texture/b4353fd0f86314353876586075b9bdf0c484aab0331b872df11bd564fcb029ed"),
	Mailbox("Mailbox", "http://textures.minecraft.net/texture/dacbbca567372a9b2b36c8f68154851bda5ee1d53e2bc208a1152d9a18d2cb"),
	;
	
	public String name;
	public String link;
	
	private Alphabet(String name, String link) {
		this.name = name;
		this.link = link;
	}
	
	public String getName() {
		return name;
	}
	
	public String getLink() {
		return link;
	}

}
