package fr.discowzombie.paragoncore.menus;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.discowzombie.paragoncore.ParagonCore;

public class InventoryManager implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent event){
		Player player = (Player) event.getWhoClicked();
		Inventory inv = event.getInventory();
		ItemStack current = event.getCurrentItem();
		InventoryAction action = event.getAction();
		if(event.getCurrentItem() == null) return;
		ParagonCore.getInstance().registeredMenus.values().stream()
		.filter(menu -> inv.getName().equalsIgnoreCase(menu.name(player)))
		.forEach(menu -> {
			menu.onClick(player, inv, action, current, event.getSlot());
			event.setCancelled(true);
		});
	}

	public void addMenu(CustomInventory m){
		ParagonCore.getInstance().registeredMenus.put(m.getClass(), m);
	}

	public void open(Player player, Class<? extends CustomInventory> gClass){
		if(!ParagonCore.getInstance().registeredMenus.containsKey(gClass)) return;
		CustomInventory menu = ParagonCore.getInstance().registeredMenus.get(gClass);
		Inventory inv = Bukkit.createInventory(null, menu.getSize(), menu.name(player));
		menu.contents(player, inv);
		player.openInventory(inv);
	}

}
