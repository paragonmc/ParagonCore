package fr.discowzombie.paragoncore.menus;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public interface CustomInventory {
	
	public abstract String name(Player p);
	
	public abstract void contents(Player p, Inventory inv);
	
	public abstract void onClick(Player p, Inventory inv, InventoryAction action, ItemStack current, int slot);
	
	public abstract int getSize();

}
