package fr.discowzombie.paragoncore.menus.inv;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.discowzombie.paragoncore.ParagonCore;
import fr.discowzombie.paragoncore.heads.Alphabet;
import fr.discowzombie.paragoncore.menus.CustomInventory;
import fr.discowzombie.paragoncore.utils.CustomSkull;
import fr.discowzombie.paragoncore.utils.ItemBuilder;

public class VoteMenu implements CustomInventory {

	int[] vertClair = { 0, 1, 7, 8, 9, 17, 18, 26, 27, 35, 36, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53 };
	int[] vertFonc = { 10, 11, 12, 13, 14, 15, 16, 19, 25, 28, 29, 30, 31, 32, 33, 34, 39, 41 };
	
	@Override
	public String name(Player p) {
		return "Votes";
	}

	@Override
	public void contents(Player p, Inventory inv) {
		for(int i : vertClair){
			inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).data((byte)5).create());
		}
		for(int i : vertFonc){
			inv.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).data((byte)13).create());
		}
		inv.setItem(2, CustomSkull.getCustomSkull(Alphabet.V.getLink()));
		inv.setItem(3, CustomSkull.getCustomSkull(Alphabet.O.getLink()));
		inv.setItem(4, CustomSkull.getCustomSkull(Alphabet.T.getLink()));
		inv.setItem(5, CustomSkull.getCustomSkull(Alphabet.E.getLink()));
		inv.setItem(6, CustomSkull.getCustomSkull(Alphabet.S.getLink()));
		
		/*VoteManager pvm = new VoteManager(p.getUniqueId());
		for(VSites vsites : VSites.values()){
			if(pvm.canVote(vsites)){
				inv.addItem(new ItemBuilder(Material.STORAGE_MINECART).name("§6Vot§ pour ParagonMc sur "+vsites.getSiteName()).create());
			}else{
				long in = pvm.canVoteIn(vsites);
				String readable = ConverterUtils.toReadableTimeFormat(in);
				inv.addItem(new ItemBuilder(Material.MINECART).name("§6Vot§ pour ParagonMc sur "+vsites.getSiteName()).lore("§7D§lai avant prochain vote: ", readable).create());
			}*/
			inv.addItem(new ItemBuilder(Material.STORAGE_MINECART).name("§6Vot§ pour ParagonMc").create());
		//}
		
		inv.setItem(37, CustomSkull.getCustomSkull(Alphabet.Twitter.getLink()));
		inv.setItem(38, CustomSkull.getCustomSkull(Alphabet.Facebook.getLink()));
		//inv.setItem(40, new ItemBuilder(Material.ARROW).create());
		inv.setItem(40, new ItemBuilder(Material.STAINED_GLASS_PANE).data((byte)5).create());
		inv.setItem(42, CustomSkull.getCustomSkull(Alphabet.Youtube.getLink()));
		inv.setItem(43, new ItemBuilder(Material.BARRIER).create());
	}

	@Override
	public void onClick(Player p, Inventory inv, InventoryAction action, ItemStack current, int slot) {
		if(current != null){
			
			if(current.getType() == Material.MINECART){
				p.playSound(p.getLocation(), Sound.BLOCK_ANVIL_BREAK, 20f, 20f);
			/*}else if(current.getType() == Material.ARROW){
				p.closeInventory();
				InventoryManager invv = new InventoryManager();
				invv.open(p, RewardsMenu.class);*/
			}else if(current.getType() == Material.STORAGE_MINECART){
				/*VSites vsites = null;
				switch (slot) {
				case 20: vsites = VSites.SITE1; break;
				case 21: vsites = VSites.SITE2; break;
				case 22: vsites = VSites.SITE3; break;
				case 23: vsites = VSites.SITE4; break;
				case 24: vsites = VSites.SITE5; break;
				default: break;
				}
				if(vsites != null){
					p.sendMessage("§aGo voter sur §6"+vsites.getUrl()+"§a.");
					if(vsites.equals(VSites.SITE1)){
						ServeurMinecraftOrg.addToWaitVote(p);
					}
					p.closeInventory();
				}*/
				p.sendMessage("§ePour voter, rendez vous sur le site de ParagonMc : §6http://www.paragonmc.fr/votes§e.");
				p.closeInventory();
			}else if(current.getItemMeta().equals(CustomSkull.getCustomSkull(Alphabet.Twitter.getLink()).getItemMeta())){
				p.closeInventory();
				p.sendMessage(ParagonCore.getInstance().getConfig().getString("messages.twitter").replace('&', '§'));
			}else if(current.getItemMeta().equals(CustomSkull.getCustomSkull(Alphabet.Facebook.getLink()).getItemMeta())){
				p.closeInventory();
				p.sendMessage(ParagonCore.getInstance().getConfig().getString("messages.facebook").replace('&', '§'));
			}else if(current.getItemMeta().equals(CustomSkull.getCustomSkull(Alphabet.Youtube.getLink()).getItemMeta())){
				p.closeInventory();
				p.sendMessage(ParagonCore.getInstance().getConfig().getString("messages.youtube").replace('&', '§'));
			}
		}
	
	}

	@Override
	public int getSize() {
		return 54;
	}

}
