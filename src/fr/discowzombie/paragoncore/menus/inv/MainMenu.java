package fr.discowzombie.paragoncore.menus.inv;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.discowzombie.paragoncore.menus.CustomInventory;
import fr.discowzombie.paragoncore.utils.ItemBuilder;
import fr.discowzombie.paragoncore.utils.RanksListManager;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.PermissionUser;

public class MainMenu implements CustomInventory {

	@Override
	public String name(Player p) {
		return "Liste des grades";
	}

	@SuppressWarnings("deprecation")
	@Override
	public void contents(Player p, Inventory inv) {
		Set<PermissionGroup> pg = new RanksListManager().getRanks().keySet();
		for(PermissionGroup g : pg){
			//PAPER = Joueurs/Achetable
			//BOOK (Livre) = Staff
			if(g.getWeight() <= 22){
				inv.addItem(new ItemBuilder(Material.ENCHANTED_BOOK)
						.name("§e"+g.getName())
						.lore("§7Prefix: §f"+g.getPrefix().replace('&', '§'), 
								"§7Poids: §f"+g.getWeight(), 
								"§7Parents: §f"+displayStringTab(g.getParentGroupsNames()), 
								"§7Membres: §f"+displaySet(g.getUsers()))
						.create());
			}else if((g.getWeight() >= 23) && (g.getWeight() <= 26)){
				inv.addItem(new ItemBuilder(Material.BOOK)
						.name("§e"+g.getName())
						.lore("§7Prefix: §f"+g.getPrefix().replace('&', '§'), 
								"§7Poids: §f"+g.getWeight(), 
								"§7Parents: §f"+displayStringTab(g.getParentGroupsNames()), 
								"§7Membres: §f"+displaySet(g.getUsers()))
						.create());
			}else{
				inv.addItem(new ItemBuilder(Material.PAPER)
						.name("§e"+g.getName())
						.lore("§7Prefix: §f"+g.getPrefix().replace('&', '§'), 
								"§7Poids: §f"+g.getWeight(), 
								"§7Parents: §f"+displayStringTab(g.getParentGroupsNames()), 
								"§7Membres: §f"+displaySet(g.getUsers()))
						.create());
			}
		}
	}

	@Override
	public void onClick(Player p, Inventory inv, InventoryAction action, ItemStack current, int slot) {
		if((current.getType() != null) && (current.getItemMeta().hasDisplayName())){
			String name = current.getItemMeta().getDisplayName().replace("&e", "").replace("§e", "");
			p.closeInventory();
			p.chat("/pex group "+name);
		}
	}

	@Override
	public int getSize() {
		return 45;
	}
	
	public String displayStringTab(String[] tab){
		if(tab != null){
			String s = null;
			for(String st : tab){
				if(s == null) s = st;
				else s = s + "§7, §f" + st;
			}
			if(s == null) return "";
			return s;
		}
		return "";
	}
	
	public String displaySet(Set<PermissionUser> st){
		if(st != null){
			String s = null;
			for(PermissionUser pu : st){
				if(s == null) s = pu.getName();
				else s = s + "§7, §f" + pu.getName();
			}
			if(s == null) return "";
			return s;
		}
		return "";
	}

	public String displaySetS(Set<String> st){
		if(st != null){
			String s = null;
			for(String stt : st){
				if(s == null) s = stt;
				else s = s + "§7, §f" + stt;
			}
			if(s == null) return "";
			return s;
		}
		return "";
	}

}
