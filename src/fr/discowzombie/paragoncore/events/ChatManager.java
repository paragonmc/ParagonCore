package fr.discowzombie.paragoncore.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.wasteofplastic.askyblock.ASkyBlockAPI;

import fr.discowzombie.paragoncore.ParagonCore;
import me.clip.deluxetags.DeluxeTag;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class ChatManager implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler (priority = EventPriority.LOWEST)
	public void onChat(AsyncPlayerChatEvent e){
		e.setCancelled(true);
		
		Player p = e.getPlayer();
		int level = -1;
		String lvl = "";
		String tag = null;
		if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
			level = ASkyBlockAPI.getInstance().getIslandLevel(p.getUniqueId());
			tag = DeluxeTag.getPlayerDisplayTag(p).replace('&', '§');
		}
		if((level / 1000) >= 1){
			double d = (level / 1000);
			lvl = String.format("%,.0f", (double)d);
			lvl = lvl + "k";
		}else{
			lvl = level+"";
		}
		String prefix = PermissionsEx.getPermissionManager().getUser(p.getUniqueId()).getPrefix().replace('&', '§');
		String suffix = PermissionsEx.getPermissionManager().getUser(p.getUniqueId()).getSuffix().replace('&', '§');
		String message = e.getMessage();
		if(p.hasPermission("paragoncore.color.admin")){
			message = "§f" + message.replace('&', '§');
		}
		boolean hasIsland = false;
		if(ASkyBlockAPI.getInstance().hasIsland(p.getUniqueId())){
			hasIsland = true;
		}
		
		for(Player all : Bukkit.getServer().getOnlinePlayers()){
			if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
				
				all.sendMessage(( hasIsland ? "§7[§f"+lvl+"§7] " : "") + prefix + ( tag == null || tag == "" ? "" : tag+" " ) + suffix + p.getName() + " §8» §f" + message);
				
			}else{
				all.sendMessage(prefix+suffix+p.getName()+" §8» §7"+message);
			}
		}
	}

}
