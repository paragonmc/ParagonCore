package fr.discowzombie.paragoncore.events;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import fr.discowzombie.paragoncore.ParagonCore;

public class PlayerCmdPreProcessEvent implements Listener {
	
	@EventHandler (priority = EventPriority.LOWEST)
	public void preCmdEvent(PlayerCommandPreprocessEvent e){
		List<String> list = ParagonCore.getInstance().getConfig().getStringList("cmd");
		if(list.contains(e.getMessage().replace("/", ""))){
			e.setCancelled(true);
			sendHelp(e.getPlayer());
		}
	}

	private void sendHelp(Player p) {
		List<String> lines = ParagonCore.getInstance().getConfig().getStringList("lines");
		for(String s : lines){
			p.sendMessage(s.replace('&', '§'));
		}
	}

}
