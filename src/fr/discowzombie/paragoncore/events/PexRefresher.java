package fr.discowzombie.paragoncore.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.discowzombie.paragoncore.ParagonCore;
import fr.discowzombie.paragoncore.manager.VoteManager;
import fr.discowzombie.paragoncore.utils.ItemBuilder;
import ru.tehkode.permissions.PermissionGroup;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class PexRefresher implements Listener {

	/**
	 * Instance de la classe principale
	 */
	private ParagonCore main;
	/**
	 * Constructeur de la class
	 * @param main Classe principale
	 */
	public PexRefresher(ParagonCore main)
	{
		this.main = main;
	}


	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		UUID uuid =  e.getPlayer().getUniqueId();


		try {
			PreparedStatement pS = main.SQL.getConnection().prepareStatement("SELECT COUNT(id) FROM joueurs WHERE uuid = ?");
			pS.setString(1, uuid.toString());
			pS.execute();
			ResultSet datalogin =  pS.getResultSet();
			if (datalogin.next()){
				int FirstConnection = datalogin.getInt(1);
				if(FirstConnection == 0){
					try {
						PreparedStatement pS2 = main.SQL.getConnection().prepareStatement("INSERT INTO `joueurs` (`uuid`, `playername` ,`status`) VALUES (?,?,?)");
						pS2.setString(1, uuid.toString());
						pS2.setString(2, e.getPlayer().getName());
						pS2.setInt(3, 1);
						pS2.execute();
						pS2.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}else{
					try {
						PreparedStatement ps3 = main.SQL.getConnection().prepareStatement("UPDATE `joueurs` SET `status`= ? WHERE (`playername`= ? )");
						ps3.setInt(1,1);
						ps3.setString(2,e.getPlayer().getName());
						ps3.execute();
						ps3.close();
					} catch (SQLException e3) {
						e3.printStackTrace();
					}
				}
			}
			pS.close();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}


		@SuppressWarnings("deprecation")
		PermissionGroup rank = PermissionsEx.getPermissionManager().getUser(p).getGroups()[0];
		p.setPlayerListName(rank.getPrefix().replace('&', '§')+p.getName());

		if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
			if(!new VoteManager(p.getUniqueId()).hasVoteStats()){
				//FIRST LOGIN
				p.getInventory().setChestplate(
						new ItemBuilder(Material.ELYTRA)
						.unbreakable(true)
						.addEnchantment(Enchantment.VANISHING_CURSE, 1)
						.addEnchantment(Enchantment.MENDING, 1)
						.create()
						);
			}
		}

		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex reload"); 
	}
	
	@EventHandler (ignoreCancelled = true)
	public void onQuit(PlayerQuitEvent e){ 

		Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), "pex reload");
		try {
			PreparedStatement ps3 = main.SQL.getConnection().prepareStatement("UPDATE `joueurs` SET `status`= ? WHERE (`playername`= ? )");
			ps3.setInt(1,0);
			ps3.setString(2,e.getPlayer().getName());
			ps3.execute();
			ps3.close();
		} catch (SQLException e3) {
			e3.printStackTrace();
		}
	}

}
