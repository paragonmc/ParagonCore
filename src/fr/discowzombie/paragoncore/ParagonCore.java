package fr.discowzombie.paragoncore;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import fr.discowzombie.paragoncore.commands.CommandsCmd;
import fr.discowzombie.paragoncore.commands.IPCmd;
import fr.discowzombie.paragoncore.commands.PexGUICmd;
import fr.discowzombie.paragoncore.commands.RulesCmd;
import fr.discowzombie.paragoncore.commands.SiteCmd;
import fr.discowzombie.paragoncore.commands.VoteCmd;
import fr.discowzombie.paragoncore.events.ChatManager;
import fr.discowzombie.paragoncore.events.PexRefresher;
import fr.discowzombie.paragoncore.events.PlayerCmdPreProcessEvent;
import fr.discowzombie.paragoncore.menus.CustomInventory;
import fr.discowzombie.paragoncore.menus.InventoryManager;
import fr.discowzombie.paragoncore.menus.inv.MainMenu;
import fr.discowzombie.paragoncore.menus.inv.VoteMenu;
import fr.discowzombie.paragoncore.scoreboard.ScoreboardManager;
import net.milkbowl.vault.economy.Economy;

public class ParagonCore extends JavaPlugin {
	
	public static ParagonCore instance;
	public HashMap<Class<? extends CustomInventory>, CustomInventory> registeredMenus = new HashMap<>();
	private static Economy econ = null;

	/**
	 * Contiens la class SQLConnection
	 */
	public SQLConnection SQL;

	public static String plugName = "ParagonCore"; //Cotiens une constante ayant le nom du pluginn



	@Override
	public void onEnable() {
		instance = this;
		
		saveDefaultConfig();
		
		setupEconomy();
		
		PluginManager pm = Bukkit.getServer().getPluginManager();
		pm.registerEvents(new InventoryManager(), this);
		pm.registerEvents(new PexRefresher(instance), this);
		pm.registerEvents(new PlayerCmdPreProcessEvent(), this);
		pm.registerEvents(new ChatManager(), this);
		pm.registerEvents(new ScoreboardManager(), this);
		
		InventoryManager inv = new InventoryManager();
		inv.addMenu(new MainMenu());
		inv.addMenu(new VoteMenu());
		
		getCommand("pexgui").setExecutor(new PexGUICmd());
		getCommand("site").setExecutor(new SiteCmd());
		getCommand("ips").setExecutor(new IPCmd());
		getCommand("commands").setExecutor(new CommandsCmd());
		getCommand("rules").setExecutor(new RulesCmd());
		getCommand("vote").setExecutor(new VoteCmd());

		try{
			SQL = new SQLConnection("jdbc:mysql://", "localhost", "padmin_web", "root", "s2+xPByqRGv!T");
			SQL.connection();
		}catch (ClassCastException e) {
			e.printStackTrace();
		}
		
		if(ParagonCore.getInstance().getConfig().getBoolean("skyblock") == true){
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
				
				@Override
				public void run() {
					ScoreboardManager.updateBoard();
				}
			}, 0L, 60L);
			
			/*
			Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
				
				@Override
				public void run() {
					ServeurMinecraftOrg.checkAll();
				}
			}, 0L, 100L);*/
		
			//Add scoreboard to all
			for(Player players : Bukkit.getServer().getOnlinePlayers()){
				ScoreboardManager.setBoard(players);
				ScoreboardManager.updateBoard();
			}
			
		}
		
		super.onEnable();
	}
	
	@Override
	public void onDisable() {
		//Remove all
		for(Player players : Bukkit.getServer().getOnlinePlayers()){
			if(ScoreboardManager.boards.containsKey(players)){
				ScoreboardManager.boards.get(players).destroy();
			}
		}
		/*for(Team t : Bukkit.getScoreboardManager().getMainScoreboard().getTeams()){
			t.unregister();
		}*/
		super.onDisable();
	}
	
	public static ParagonCore getInstance(){
		return instance;
	}
	
	private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
	
	public static Economy getEcononomy() {
        return econ;
    }

}
